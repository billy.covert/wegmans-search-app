import React, { useState, useContext } from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import HomeIcon from '@material-ui/icons/ChevronLeft';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { useHistory } from "react-router-dom"
import { useQuery } from 'react-query'
import { getStores, setStore } from './api'
import { UserContext } from './context';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  offset: theme.mixins.toolbar,
  list: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  container: {
    padding: 0
  }
}));

const filterData = (term = '') => store => {
  if(!term) {
    return true
  }
  return store.name.toLowerCase().includes(term.toLowerCase()) || store.address.postal_code.includes(term)
}

export function Stores() {
  const classes = useStyles();
  const history = useHistory()
  const { isLoading, error, data } = useQuery('store-data', getStores)
  const [term, setTerm] = useState('')
  const onChange = e => setTerm(e.target.value)

  const { setUser } = useContext(UserContext);

  const pickStore = store_id => {
    return setStore(store_id).then((user) => {
      setUser(user)
      history.push('/products')
    });
  }


  return (
    <div>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={() => history.push('/products')}>
            <HomeIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Select Store
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.offset}></div>
      <Container maxWidth="sm">
        <br/>
        <TextField fullWidth id="search" name="search" label="Search" type="text" value={term} onChange={onChange} />
        { isLoading && <div>Loading...</div>}
        { error && <div>{error.message}</div> }
        { data && data.items.filter(filterData(term)).map( ({name, address, id})  => (
          <Paper key={id} style={{marginBottom: '10px', padding: '10px', marginTop: '10px'}}>
            <Grid container justify="space-between" alignItems="center" spacing={1}>
              <Grid item xs={8} style={{padding: '5px'}}>
                <h2>{name}</h2>
                <p>{address.address1} {address.city}, {address.province} {address.postal_code}</p>
              </Grid>
              <Grid item xs={4} style={{padding: '5px', textAlign: 'right'}}>
                <Button variant="contained" color="primary" onClick={() => pickStore(id)}>
                  Select
                </Button>
              </Grid>
            </Grid>
          </Paper>
        )) }
      </Container>
    </div>
  );
}
