import React, { useState, useEffect, useContext } from 'react'
import { useHistory } from "react-router-dom"
import { BehaviorSubject, of, merge } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, filter, switchMap, catchError } from 'rxjs/operators';

import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Store';
import ClearIcon from '@material-ui/icons/HighlightOff';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import LinearProgress from '@material-ui/core/LinearProgress';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { UserContext } from './context';


import { getTerms, searchProducts } from './api'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  offset: theme.mixins.toolbar,
  list: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  container: {
    padding: 0
  },
  cardRoot: {
    display: 'flex',
    marginTop: '10px',
    marginBottom: '10px'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    minWidth: 151,
  },
}));

const stripString = s => s.replace(/(<([^>]+)>)/gi, "");

export function Products() {
  const { user } = useContext(UserContext);
  const classes = useStyles();
  const history = useHistory()

  const [term, setTerm] = useState('')

  const [state, setState] = useState({
    data: [],
    loading: false,
    errorMessage: '',
    noResults: false
  });

  const [productsState, setProductsState] = useState({
    data: {
      items: []
    },
    loading: false,
    errorMessage: '',
    noResults: false
  });

  const [subject, setSubject] = useState(null);

  useEffect(() => {
    try{
      const { results, term } = JSON.parse(localStorage.searchCache);
      setTerm(term);
      setProductsState(s => ({
        ...s,
        data: results,
        loading: false
      }))
    } catch(e) {
      console.log('unable to restore cache')
    }
  }, []);

  useEffect(() => {
    if(subject === null) {
      const sub = new BehaviorSubject('');
      setSubject(sub);
    } else {
      const observable = subject.pipe(
        map(s => s.trim()),
        distinctUntilChanged(),
        filter(s => s.length >= 2),
        debounceTime(200),
        switchMap(term =>
          merge(
            of({loading: true, errorMessage: '', noResults: false}),
            getTerms(term).then(terms => ({
              data: terms,
              loading: false,
              noResults: terms.length === 0
            }))
          )
        ),
        catchError(e => ({
          loading: false,
          errorMessage: e.response ? e.response.data.message : e.message
        }))
      ).subscribe(ns => setState(s => Object.assign({}, s, ns)));

      return () => {
        observable.unsubscribe()
        subject.unsubscribe();
      }
    }
  }, [subject]);

  const onChange = e => {
    const t = e.target.value;
    setTerm(t)
    if(subject) {
      return subject.next(t);
    }
  };

  const onSearchClick = t => {
    const st = stripString(t);
    setTerm(st)
    setState(s => Object.assign({}, s, {data: []}))
    setProductsState(s => ({
      ...s,
      loading: true,
      errorMessage: ''
    }))
    return searchProducts(st).then(data => {
      localStorage.searchCache = JSON.stringify({
        results: data,
        term: st
      });

      setProductsState(s => ({
        ...s,
        data,
        loading: false
      }))
    }).catch(e => {
      setProductsState(s => ({
        ...s,
        errorMessage: e.message,
        loading: false
      }))
    });
  }

  const onSubmit = e => {
    e.preventDefault();
    return onSearchClick(term);
  };

  const clearSearch = () => {
    const results = { items: []};
    setTerm('')
    setProductsState(s => ({
      ...s,
      data: results,
      loading: false
    }))
    localStorage.searchCache = JSON.stringify({
      results,
      term: ''
    });
  }

  return (
    <div>
      <AppBar position="fixed">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Products
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.offset}></div>
      <Container maxWidth="sm">
        <br/>
        <Button
          variant="contained"
          color="primary"
          onClick={() => history.push('/stores')}
          startIcon={<HomeIcon />} >
          {user && user.store && user.store.name}
        </Button>
        <br/>
        <form onSubmit={onSubmit} style={{marginTop: '10px', marginBottom: '10px'}}>
          <Input
            id="search"
            name="search"
            fullWidth
            type="text"
            value={term}
            onChange={onChange}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="clear search"
                  onClick={clearSearch}>
                  <ClearIcon />
                </IconButton>
              </InputAdornment>
            } />
        </form>
        { state.loading && <div>Loading...</div>}
        { state.errorMessage && <div>{state.errorMessage}</div> }
        { state.data && state.data.map( result => (
          <div key={result} style={{padding: '10px'}} onClick={() => onSearchClick(result)}>
            <div dangerouslySetInnerHTML={{__html: result}}></div>
          </div>
        )) }

        { productsState.loading && <LinearProgress /> }
        { productsState.errorMessage && <div>{productsState.errorMessage}</div> }
        { productsState.data && productsState.data.items.map( ({aisle, images, name, id, categories, base_price})  => (
          <Card key={id} className={classes.cardRoot} onClick={() => history.push(`/stores/${user.store.id}/products/${id}`)}>
            <CardMedia
              className={classes.cover}
              image={images.tile.large}
              title="Product Pic"
            />
            <div className={classes.details}>
              <CardContent className={classes.content}>
                <Typography variant="h6">
                  {name}
                </Typography>
                <Typography variant="body1" color="textSecondary">
                  {aisle}
                </Typography>
                <Typography variant="body1">
                  $ {base_price}
                </Typography>
              </CardContent>
            </div>
          </Card>
        )) }
      </Container>
    </div>
  );
}
