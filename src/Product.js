import React, { useContext } from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton';
import HomeIcon from '@material-ui/icons/ChevronLeft';
import Button from '@material-ui/core/Button';
import ShareIcon from '@material-ui/icons/Share';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useHistory, useParams } from "react-router-dom"
import { useQuery } from 'react-query'
import { UserContext } from './context';
import { getProduct } from './api'

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  offset: theme.mixins.toolbar,
  list: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  container: {
    padding: 0
  }
}));

export const ProductCard = ({images = {tile: {}}, name, aisle, base_price} = {}) => {
  return (
    <Paper style={{marginBottom: '10px', padding: '10px', marginTop: '10px'}}>
      <Grid container justify="space-between" alignItems="center" spacing={1}>
        <Grid item xs={4} style={{padding: '5px', textAlign: 'right'}}>
          <img src={images.tile.large} style={{width: '100%', height: 'auto'}} alt={name} />
        </Grid>
        <Grid item xs={8} style={{padding: '5px'}}>
          <h2>{name}</h2>
          <p>
            <strong>{aisle}</strong>
            <br/>
            {base_price}
          </p>
        </Grid>
      </Grid>
    </Paper>
  );
}

export const ShareProduct = ({name}) => {
  return (
    <Button
      variant="contained"
      color="default"
      onClick={() => navigator.share({
        title: 'Wegmans Product',
        text: name,
        url: window.location.href
      })}
      startIcon={<ShareIcon />}>
      Share
    </Button>
  )
}

export const ProductInfo = ({description, ingredients, instructions, nutrition}) => {
  return (
    <div>
      <h2>Description</h2>
      <p style={{whiteSpace: 'pre-wrap'}}>{description}</p>

      { ingredients && <>
        <h2>Ingredients</h2>
        <p style={{whiteSpace: 'pre-wrap'}}>{ingredients}</p>
      </> }

      { instructions && <>
        <h2>Instructions</h2>
        <p style={{whiteSpace: 'pre-wrap'}}>{instructions}</p>
      </> }

      { nutrition && <>
        <h2>Nutrition</h2>
        <pre>{JSON.stringify(nutrition.data, null, 2)}</pre>
      </> }
    </div>
  );
};



export function Product() {
  const classes = useStyles();
  const history = useHistory()
  const { user } = useContext(UserContext);
  const { id, storeId } = useParams();
  const { isLoading, error, data } = useQuery(`product-${id}`, () => getProduct(id, storeId, user))

  return (
    <div>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={() => history.push('/products')}>
            <HomeIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Product
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.offset}></div>
      <Container maxWidth="sm">
        { isLoading && <> <br/> <LinearProgress /> </>}
        { error && <div>{error.message}</div> }
        { !isLoading && !error && (<>
        <ProductCard {...data} />
        { navigator.share && <ShareProduct name={data.name} /> }
        <ProductInfo {...data} />
        </>)}
      </Container>
    </div>
  );
}
