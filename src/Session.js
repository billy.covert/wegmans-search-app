import React, { useEffect, useState } from 'react';
import {
  getUser,
  setAuthorization,
  postUserSession,
  postUser
} from './api';
import CircularProgress from '@material-ui/core/CircularProgress';
import { UserContext } from './context';

const tokenKey = 'session-token'

const isSessionValid = () => {
  const cachedToken = localStorage[tokenKey];
  if(!cachedToken){
    return Promise.resolve(false)
  }
  setAuthorization(cachedToken)

  return getUser().then((user) => {
    return user
  }).catch(() => {
    console.log('error getting user - session invalid')
    return false
  });
}

const createSession = () => {
  return postUserSession().then(() => {
    return postUser().then((data) => {
      localStorage[tokenKey] = data.session_token;
      return data;
    });
  });
}

const startSession = () => {
  return isSessionValid().then(user => {
    if(user){
      return user;
    }
    return createSession();
  })
}


export const Session = ({children}) => {
  const [state, setState] = useState({
    loading: true
  })

  const [user, setUser] = useState({})

  const contextValue = {user, setUser}

  useEffect(() => {
    console.log('on mount - check session')
    setState({loading: true})
    startSession().then((userFromApi) => {
      setUser(userFromApi)
      console.log('session started')
      setState({loading: false})
    }).catch(e => {
      setState({
        loading: false,
        errorMessage: e.message
      })
    });
  }, []);

  return <UserContext.Provider value={contextValue}>
    { state.loading ? <div style={{display: 'flex', justifyContent: 'center', paddingTop: '2rem', alignItems: 'center'}} > <CircularProgress /> </div>  : null }
    { state.errorMessage ? <div>{state.errorMessage}</div> : null }
    { !state.loading && !state.errorMessage && children }
  </UserContext.Provider>
};
