import * as React from 'react';
import { Stores } from './Stores';
import { Session } from './Session';
import { Products } from './Products';
import { Product } from './Product';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App() {
  return (
    <Session>
      <Switch>
        <Route path="/stores/:storeId/products/:id">
          <Product />
        </Route>
        <Route path="/stores">
          <Stores />
        </Route>
        <Route path="/products">
          <Products />
        </Route>
        <Route>
          <Redirect to="/products" />
        </Route>
      </Switch>
    </Session>
  );
}

export default App;
