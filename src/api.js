import axios from 'axios'

export const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL
});

export const setAuthorization = token => api.defaults.headers.common['Authorization'] = `Bearer ${token}`;

export const getUser = () => api.get('user').then(({data}) => data)

const session = {
  binary: "cms-module",
  binary_version: "release-1.0.0",
  is_retina:false,
  os_version:"MacIntel",
  pixel_density:"2.0",
  push_token:"",
  screen_height:900,
  screen_width:1440
};

export const postUserSession = () => api.post('user_sessions', session).then(({data}) => {
  setAuthorization(data.session_token)
  return data
});

export const postUser = () => api.post('users', {}).then(({data}) => {
  setAuthorization(data.session_token)
  return data
});

export const setStore = store_id => api.patch('user', {store_id}).then(({data}) => data)

export const getStores = () => api.get('stores').then(({data}) => data)

export const getTerms = term => api.get('autocomplete', {params: { search_term: term}}).then(({data}) => data.product_autocompletes)

export const searchProducts = term => api.get(
  'store_products',
  {params: {
    sort: 'rank',
    limit: 60,
    offset: 0,
    search_term: term
  }}
).then(({data}) => data)

export const getProduct = (id, storeId, user) => {
  return Promise.resolve().then(() => {
    console.log('storeId: ', storeId, 'store.id: ', user.store.id)
    if (storeId && user && user.store && user.store.id !== storeId) {
      console.log('stores are different or not set - set store to', storeId)
      return setStore(storeId);
    }
    return;
  }).then(() => {
    return api.get(`store_products/${id}`).then(({data}) => data)
  });
};

