const app = require('./app.js')

app.listen(8181, function () {
  console.log('CORS-enabled web server listening on port 8181')
})
