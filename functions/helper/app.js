const express = require('express')
const proxy = require('express-http-proxy');
const cors = require('cors')
const morgan = require('morgan')
const app = express()

app.use(morgan('combined'))
app.use(cors())

app.use((req, res, next) => {
  console.log('req headers', req.headers);
  delete req.headers['x-request-id'];
  next();
});

app.use('/.netlify/functions/proxy/', proxy('https://shop.wegmans.com'))

module.exports = app
